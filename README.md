
Tradução para pt-br e melhorias no mushclient.
Devil's Bob MUD, 2019.

Este trabalho é dedicado focado para toda a comunidade de deficientes visuais 
e é uma tentativa de ampliar a gama de opções de clientes para muds disponíveis para a comunidade.


# Como Traduzir?
A tradução deve ser feita em 2 arquivos localizados na pasta locale.

1 - Tradução de mensagens gerais

1.1 - Baixe o editor de texto notepad++ em https://notepad-plus-plus.org/download/

1.2 - Abra o arquivo PT.lua localizado na pasta locale do mushclient

1.3 - Traduza os textos que desejar.

1.3.a - Traduza sempre no lado direito do símbolo de igual: 

```
["I notice that this is the first time you have used MUSHclient on this PC."] =
 "Noto que esta é a primeira vez que você usa o MUSHclient neste dispositivo.",
```
 
1.3.b - Quando o texto possuir caracteres especiais, signifca que o texto possui variáveis.
Neste caso, traduza no campo RETURN e utilize a função 'string.format()', seguido do texto e em seguida das variáveis na mesma posição do texto original, conforme o exemplo abaixo. 

```
  ["The %s contains %i line%s, %i word%s, %i character%s"] =
    function (a, b, c, d, e, f, g)
     
      return string.format("O %s contém %i linha%s, %i palavra%s, %i caractere%s", a, b, c, d, e, f, g)
    end,  -- function
```

 1.4 Salve o arquivo.
*IMPORTANTE - Feche o MUSHClient antes de salvar seu trabalho.*

2 - Tradução de menus e telas: 

2.1 - Baixe o programa http://www.resedit.net/

2.2 - Abra o programa ResEdit.exe

2.3 - Feche o diálogo de 'wizard' que abrir.

2.4 - No menu, vá em FILE, OPEN PROJECT

2.5 - Selecione o arquivo PT.dll localizado na lasta 'locale' no mushclient

2.6 - Edite o que precisar. Note que é possível editar figuras, imagens, menus, diálogos e outras mensagens.

2.7 - Aperte ctrl+s para salvar suas alterações.

*IMPORTANTE - Feche o MUSHClient antes de salvar seu trabalho.*